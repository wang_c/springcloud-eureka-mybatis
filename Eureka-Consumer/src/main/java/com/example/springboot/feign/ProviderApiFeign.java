package com.example.springboot.feign;

import com.example.springboot.util.PageRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangc
 * @date 2020.7.2
 */
@FeignClient(name = "Eureka-Provider")
public interface ProviderApiFeign {

    /**
     * 根据id查找用户，url要与provider的接口相同
     * @param id
     * @return
     */
    @RequestMapping("user/getUserById/{id}")
    public String getUserById(@PathVariable int id);

    @PostMapping("/user/listUserByPage")
    public Object listUserByPage(@RequestBody PageRequest pageQuery);

    @DeleteMapping("/user/deleteUserById/{id}")
    public int deleteUserById(@PathVariable int id);

    @PutMapping("/user/updateUserById/{id}")
    public int updateUserById(@PathVariable int id);

    @PostMapping("/user/insertUser")
    public int insertUser();
}
