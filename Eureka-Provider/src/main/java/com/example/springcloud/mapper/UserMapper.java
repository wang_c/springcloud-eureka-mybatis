package com.example.springcloud.mapper;

import com.example.springcloud.entity.UserDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author wangc
 * @date 2020.7.2
 */
@Repository
public interface UserMapper {

    /**
     * 通过用户id查询user
     * @param id
     * @return
     */
    UserDO getById(int id);

    /**
     * 分页查询
     * @return
     */
    List<UserDO> listByPage();

    /**
     * 通过id删除user
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 通过id更新user
     * @param id
     * @return
     */
    int updateById(int id);

    /**
     * 插入一个user
     * @param userDO
     * @return
     */
    int insertUser(UserDO userDO);
}
