package com.example.springcloud.seviceImpl;

import com.example.springcloud.entity.UserDO;
import com.example.springcloud.mapper.UserMapper;
import com.example.springcloud.service.UserService;
import com.example.springcloud.util.PageRequest;
import com.example.springcloud.util.PageResult;
import com.example.springcloud.util.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserDO selById(int id){
        return userMapper.getById(id);
    }

    /**
     * 分页查询
     * @param pageRequest
     * @return
     */
    @Override
    public PageResult listUserByPage(PageRequest pageRequest){
        return PageUtils.getPageResult(pageRequest, getPageInfo(pageRequest));
    }

    /**
     * 获取pageInfo
     * @param pageRequest
     * @return
     */
    private PageInfo<UserDO> getPageInfo(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<UserDO> Menus = userMapper.listByPage();
        return new PageInfo<UserDO>(Menus);
    }

    /**
     * 删除user
     * @param id
     * @return
     */
    @Override
    public int deleteUserById(int id){
        return userMapper.deleteById(id);
    }

    /**
     * 更新user信息
     * @param id
     * @return
     */
    @Override
    public int updateUserById(int id){
        return userMapper.updateById(id);
    }

    /**
     * 插入一个user
     * @param userDO
     * @return
     */
    @Override
    public int insertUser(UserDO userDO){
        return userMapper.insertUser(userDO);
    }
}
