package com.example.springcloud.controller;

import com.example.springcloud.entity.UserDO;
import com.example.springcloud.service.UserService;
import com.example.springcloud.util.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangc
 * @Date 2020.7.2
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 通过id获取user
     * @param id
     * @return user.toString()
     */
    @GetMapping("/getUserById/{id}")
    public String getUserById(@PathVariable int id){
        return userService.selById(id).toString();
    }

    /**
     * 分页查询
     */
    @PostMapping(value="/listUserByPage")
    public Object findPage(@RequestBody PageRequest pageQuery) {
        return userService.listUserByPage(pageQuery);
    }

    /**
     * 通过id删除user
     * @param id
     * @return
     */
    @DeleteMapping("/deleteUserById/{id}")
    public int deleteUserById(@PathVariable int id){
        return userService.deleteUserById(id);
    }

    /**
     * 通过id更新user
     * @param id
     * @return
     */
    @PutMapping("/updateUserById/{id}")
    public int updateUserById(@PathVariable int id){
        return userService.updateUserById(id);
    }

    /**
     * 插入一个用户
     * @param userDO
     * @return
     */
    @PostMapping("/insertUser")
    public int insertUser(UserDO userDO){
        return userService.insertUser(userDO);
    }
}