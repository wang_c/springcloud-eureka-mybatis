package com.example.springboot.util;

import lombok.Data;

@Data
public class PageRequest {

    private int PageNum;

    private int PageSize;
}
