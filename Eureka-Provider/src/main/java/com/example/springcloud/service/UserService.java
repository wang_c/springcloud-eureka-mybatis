package com.example.springcloud.service;

import com.example.springcloud.entity.UserDO;
import com.example.springcloud.util.PageRequest;
import com.example.springcloud.util.PageResult;

public interface UserService {

    UserDO selById(int id);

    PageResult listUserByPage(PageRequest pageRequest);

    int deleteUserById(int id);

    int updateUserById(int id);

    int insertUser(UserDO userDO);
}
