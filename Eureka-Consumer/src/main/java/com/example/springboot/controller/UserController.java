package com.example.springboot.controller;

import com.example.springboot.feign.ProviderApiFeign;
import com.example.springboot.util.PageRequest;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author wangc
 * @date 2020.7.2
 */
@RestController
public class UserController {

    @Autowired(required = false)
    private RestTemplate restTemplate;

    @Autowired
    private ProviderApiFeign providerApiFeign;

    //方法一，通过rest
    @RequestMapping("/getUserByIdRest/{id}")
    public String getUserByIdRest(@PathVariable int id){
        String result=restTemplate.getForObject("http://Eureka-Provider:8001/user/getUserById/id",String.class);
        return result;
    }

    //方法二，通过feign

    /**
     * 通过id查找用户
     * @param id
     * @return
     */
    @RequestMapping("/getUserById/{id}")
    public String getUserById(@PathVariable int id){
        return providerApiFeign.getUserById(id);
    }

    /**
     * 分页查询
     * @param pageQuery
     * @return
     */
    @PostMapping("/listUserByPage")
    public Object findPage(@RequestBody PageRequest pageQuery){
        return providerApiFeign.listUserByPage(pageQuery);
    }

    /**
     * 根据id删除用户
     * @param id
     * @return
     */
    @DeleteMapping("/deleteUserById/{id}")
    public int deleteUserById(@PathVariable int id){
        return providerApiFeign.deleteUserById(id);
    }

    /**
     * 根据id更新用户
     * @param id
     * @return
     */
    @PutMapping("/updateUserById/{id}")
    public int updateUserById(@PathVariable int id){
        return providerApiFeign.updateUserById(id);
    }

    /**
     * 插入用户
     * @return
     */
    @PostMapping("/insertUser")
    public int insertUser(){
        return providerApiFeign.insertUser();
    }
}